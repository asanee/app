package asanee.tosaganjana.kku.ac.th.testclass.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import asanee.tosaganjana.kku.ac.th.testclass.ListStudentActivity;
import asanee.tosaganjana.kku.ac.th.testclass.R;



public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {

    private List<index> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mTitle;
        public TextView mAuthor;
        public TextView mIndex;
        public Button btnComment;

        public ViewHolder(View view) {
            super(view);

            mTitle = (TextView) view.findViewById(R.id.title);
//            mAuthor = (CustomTextView) view.findViewById(R.id.author);
            mIndex = (TextView) view.findViewById(R.id.index);
            btnComment = (Button) view.findViewById(R.id.btn_comment);
            btnComment.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d("test", String.valueOf(getAdapterPosition()));
        }
    }

    public IndexAdapter(Context context, List<index> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        index index = mIndices.get(position);

        viewHolder.mIndex.setText(String.valueOf(index.getIndex()));
        viewHolder.mTitle.setText(index.getTitle());
//        viewHolder.mAuthor.setText("โดย : " + index.getAuthor());
        viewHolder.btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /// button click event
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference myRef = database.getReference();

                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        DataSnapshot student = dataSnapshot.child("project").child("key").child("joint");
                        int count = 0;
                        for (final DataSnapshot child : student.getChildren()) {
                            count++;
                            if (count == position + 1) {
                                Log.d("child", child.getKey());

                                String name = dataSnapshot.child("user")
                                        .child( child.getKey()).child("name").getValue(String.class);

                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(mContext);

                                LayoutInflater inflater = (LayoutInflater) mContext
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                                View viewDialog = inflater.inflate(R.layout.dialog_custom, null);
                                builder.setView(viewDialog);

                                final EditText comment = (EditText) viewDialog.findViewById(R.id.comment);
                                TextView textView = (TextView) viewDialog.findViewById(R.id.name_dialog);
                                textView.setText(name);
                                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Check username password
                                        if (!comment.getText().toString().equals("")) {

                                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                                            final DatabaseReference myRef = database.getReference();

                                            myRef.child("project").child("key").child("joint")
                                                    .child(child.getKey()).child("comment")
                                                    .setValue(comment.getText().toString());

                                        }


                                    }
                                });
                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });

                                builder.create();

                                builder.show();


                            }
                        }
                    }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

        });
    }


    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}