package asanee.tosaganjana.kku.ac.th.testclass;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.testclass.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.testclass.index.RecyclerItemClickListener;
import asanee.tosaganjana.kku.ac.th.testclass.index.index;

public class ListStudentActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<index> dataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_student);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference();

//        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
//                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, final int position) {
//                // ...
//
////                Toast.makeText(getApplicationContext(), String.valueOf(position + 1), Toast.LENGTH_SHORT).show();
////                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
////                    @Override
////                    public void onDataChange(DataSnapshot dataSnapshot) {
////                        DataSnapshot student = dataSnapshot.child("project").child("key").child("joint");
////                        int count = 0;
////                        for (final DataSnapshot child : student.getChildren()) {
////                            count++;
////                            if (count == position + 1) {
////                                Log.d("child", child.getKey());
////
////                                String name = dataSnapshot.child("user").child( child.getKey()).child("name").getValue(String.class);
////
////                                AlertDialog.Builder builder =
////                                        new AlertDialog.Builder(ListStudentActivity.this);
////                                LayoutInflater inflater = getLayoutInflater();
////
////                                View viewDialog = inflater.inflate(R.layout.dialog_custom, null);
////                                builder.setView(viewDialog);
////
////                                final EditText comment = (EditText) viewDialog.findViewById(R.id.comment);
////                                TextView textView = (TextView) viewDialog.findViewById(R.id.name_dialog);
////                                textView.setText(name);
////                                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
////                                    @Override
////                                    public void onClick(DialogInterface dialog, int which) {
////                                        // Check username password
////                                        if (!comment.getText().toString().equals("")) {
////
////                                            FirebaseDatabase database = FirebaseDatabase.getInstance();
////                                            final DatabaseReference myRef = database.getReference();
////
////                                            myRef.child("project").child("key").child("joint")
////                                                    .child(child.getKey()).child("comment")
////                                                    .setValue(comment.getText().toString());
////
////                                        }
////
////
////                                    }
////                                });
////                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
////                                    @Override
////                                    public void onClick(DialogInterface dialog, int which) {
////
////                                    }
////                                });
////
////                                builder.create();
////
////                                builder.show();
////
////
////                            }
////                        }
////                    }
////
////
////                    @Override
////                    public void onCancelled(DatabaseError databaseError) {
////
////                    }
////                });
//            }
//
//            @Override
//            public void onItemLongClick(View view, int position) {
//                // ...
//            }
//        }));


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataset = new ArrayList<index>();

                DataSnapshot student = dataSnapshot.child("project").child("key").child("joint");
                DataSnapshot user = dataSnapshot.child("user");
                int i = 1;
                for (DataSnapshot child : student.getChildren()) {
                    //Here you can access the child.getKey()
                    String studentKey = child.getKey();

                    for (DataSnapshot key : user.getChildren()) {
                        //Here you can access the child.getKey()
                        if (studentKey.equals(key.getKey())) {

                            String name = user.child(key.getKey()).child("name").getValue(String.class);
                            index item = new index(i, name, "Author " + i);
                            i++;
                            dataset.add(item);
                            Log.d("key", String.valueOf(i));
                        }
                        //Log.d("key",child.getKey());
                    }

//                    Log.d("key",child.getKey());
                }

                mLayoutManager = new LinearLayoutManager(getApplicationContext());
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new IndexAdapter(ListStudentActivity.this, initIndex());
                mRecyclerView.setAdapter(mAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private List<index> initIndex() {

        return dataset;
    }


}
