package asanee.tosaganjana.kku.ac.th.testclass;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CurrentLocationActivity extends AppCompatActivity implements
        ConnectionCallbacks,
        OnConnectionFailedListener,
        LocationListener,
        View.OnClickListener {


    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private TextView tvCurrentLat, tvCurrentLnt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);

        Button btnGetLocation = (Button) findViewById(R.id.get_location);
        Button btnCreateClass = (Button) findViewById(R.id.create_class);
        Button btnSignOut = (Button) findViewById(R.id.sign_out);
        Button btnShowStudent = (Button) findViewById(R.id.student);
        Button btnDialog = (Button) findViewById(R.id.dialog);

        btnGetLocation.setOnClickListener(this);
        btnCreateClass.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
        btnShowStudent.setOnClickListener(this);
        btnDialog.setOnClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        setTitle("CurrentLocationDisplay");

        tvCurrentLat = (TextView) findViewById(R.id.cur_lat);
        tvCurrentLnt = (TextView) findViewById(R.id.cur_lnt);


    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


    }

    /**
     * If connected get lat and long
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();


            tvCurrentLat.setText(String.valueOf(currentLatitude));
            tvCurrentLnt.setText(String.valueOf(currentLongitude));

            Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        tvCurrentLat = (TextView) findViewById(R.id.cur_lat);
        tvCurrentLnt = (TextView) findViewById(R.id.cur_lnt);

        //Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.get_location) {
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("project").child("key")
                    .child("joint").child("student");

            float[] results = new float[1];
            Location.distanceBetween(100, 100, 100,
                    100, results);
            float distanceInMeters = results[0];
            boolean isWithin10km = distanceInMeters < 100;

            Log.d("lat", String.valueOf(isWithin10km));

            if(isWithin10km){
                final DatabaseReference time = myRef.child("time");
                time.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        int count = dataSnapshot.getValue(int.class);
                        count++;

                        time.setValue(count);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            DatabaseReference location = myRef.child("location");

            location.child("lat").setValue(currentLatitude);
            location.child("lng").setValue(currentLongitude);

            Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();

        } else if (id == R.id.sign_out) {

            FirebaseAuth.getInstance().signOut();

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

        } else if (id == R.id.create_class) {

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("project");

            final String key = myRef.push().getKey();

            final String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

            myRef.child(key).child("master").setValue(UID);

            /** Create project Class **/

            DatabaseReference mCreate = myRef.child(key).child("create");

            mCreate.child("location").child("lat").setValue(currentLatitude);
            mCreate.child("location").child("lng").setValue(currentLongitude);

            mCreate.child("time").setValue("00:00");
            mCreate.child("time range").child("final").setValue("00-00-0000");
            mCreate.child("time range").child("start").setValue("00-00-0000");

            mCreate.child("วันเรียน").child("date").child("day").setValue("mon");

           final DatabaseReference user = database.getReference("user");

            user.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot data : dataSnapshot.getChildren()){
                        long maxNumClass = data.getChildrenCount();
                        Log.d("Test", String.valueOf(data.getChildrenCount()));

                        user.child(UID).child("class-" + maxNumClass).setValue(key);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });


            Toast.makeText(this, "Create Class success!!!", Toast.LENGTH_LONG).show();

        } else if(id == R.id.student){
            Intent intent = new Intent(this,ListStudentActivity.class);
            startActivity(intent);
        } else if(id == R.id.dialog){

        }
    }
}
