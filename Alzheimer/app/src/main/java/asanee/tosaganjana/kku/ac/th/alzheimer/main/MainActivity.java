package asanee.tosaganjana.kku.ac.th.alzheimer.main;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.medicine.MedicineFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.outside.OutsideFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.personal.PersonalFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.queue.QueueFragment;
import asanee.tosaganjana.kku.ac.th.alzheimer.service.MyService;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase.Uid;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.*;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static StorageReference mStorageRef;

    CircleImageView imageView;
    StorageReference imageRef;
    public static String UID;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(5);

        /******************************************************************************************
         * NavigationView
         ******************************************************************************************
         */
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        final TextView tvName = (TextView) headerView.findViewById(R.id.nav_name_tv);
        final TextView tvID = (TextView) headerView.findViewById(R.id.nav_id_tv);
        imageView = (CircleImageView) headerView.findViewById(R.id.nav_profile_image);

        /******************************************************************************************
         *  Progress
         * ****************************************************************************************
         */
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Processing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        //mProgress.show();
        /*******************************************************************************************
         * Connect FirebaseDatabase
         * *****************************************************************************************
         */
        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // Real time Database
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("personal");

        // Storage
        mStorageRef = FirebaseStorage.getInstance().getReference();

        imageRef = mStorageRef.child(Uid + "/profile.PNG");
        downloadDataViaUrl();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue(String.class);
                String id = dataSnapshot.child("id").getValue(String.class);
                String imageUrl = dataSnapshot.child("image").getValue(String.class);

                tvName.setText(name);
                tvID.setText("HN : " + id);


            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

        /**
         * replace first page to personalFragment
         */
        FragmentManager manager = getSupportFragmentManager();
        MainFragment mainFragment = new MainFragment();
        manager.beginTransaction().replace(
                R.id.content_main_fragment,
                mainFragment,
                mainFragment.getTag()
        ).commit();

        /***************************************************
         * Start service
         ***************************************************
         */
        Intent intent = new Intent(this, MyService.class);
        startService(intent);

        /**************************************************
         * select case to intent form notification massage
         **************************************************
         */

        if (isNotificationMassage || isSetting) {

            mProgress.dismiss();
            Bundle bundle = getIntent().getExtras();
            String message = bundle.getString("message");

            isNotificationMassage = false;

            if (message.equals("personal")) {

                isSetting = false;

                PersonalFragment personalFragment = new PersonalFragment();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        personalFragment,
                        personalFragment.getTag()
                ).commit();
            } else if (message.equals("medicine")) {

                isSetting = false;

//                MedicineFragment medicineFragment = new MedicineFragment();
//                manager.beginTransaction().replace(
//                        R.id.content_main_fragment,
//                        medicineFragment,
//                        medicineFragment.getTag()
//                ).commit();
                Intent intent1 = new Intent(this,MedicineFragment.class);
                startActivity(intent1);

            } else if (message.equals("queue")) {

                QueueFragment queueFragment = new QueueFragment();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        queueFragment,
                        queueFragment.getTag()
                ).commit();
            } else if (message.equals("outside")) {

                OutsideFragment outsideFragment = new OutsideFragment();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        outsideFragment,
                        outsideFragment.getTag()
                ).commit();
            }
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.sign_out) {
//
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_personal) {

            PersonalFragment personalFragment = new PersonalFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_main_fragment,
                    personalFragment,
                    personalFragment.getTag()
            ).commit();

            Toast.makeText(this, "Personal", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_medicine) {

//            MedicineFragment medicineFragment = new MedicineFragment();
//            FragmentManager manager = getSupportFragmentManager();
//            manager.beginTransaction().replace(
//                    R.id.content_main_fragment,
//                    medicineFragment,
//                    medicineFragment.getTag()
//            ).commit();

            Intent intent1 = new Intent(this,MedicineFragment.class);
            startActivity(intent1);

            Toast.makeText(this, "Medicine", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_queue) {

            QueueFragment queueFragment = new QueueFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_main_fragment,
                    queueFragment,
                    queueFragment.getTag()
            ).commit();

            Toast.makeText(this, "Queue", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_outside) {

            OutsideFragment outsideFragment = new OutsideFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_main_fragment,
                    outsideFragment,
                    outsideFragment.getTag()
            ).commit();

            Toast.makeText(this, "Leave", Toast.LENGTH_LONG).show();

        } else if (id == R.id.sign_out) {

            FirebaseAuth.getInstance().signOut();

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_home) {

            MainFragment mainFragment = new MainFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_main_fragment,
                    mainFragment,
                    mainFragment.getTag()
            ).commit();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void downloadDataViaUrl() {

        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                try {
                    Glide.with(MainActivity.this)
                            .load(uri)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(imageView);
                    mProgress.dismiss();
                } catch (Exception e) {
                    Log.d(TAG, "Error");
                }
                Log.d(TAG, uri + "");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

                Log.d(TAG, "Error");
            }
        });
    }
}
