package asanee.tosaganjana.kku.ac.th.alzheimer.personal;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;
import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase.Uid;
import static asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity.mStorageRef;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.*;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.*;

public class PersonalEditFragment extends Fragment {

    private static final int PICK_PHOTO_FOR_AVATAR = 1000;

    StorageReference imageRef;
    StorageReference folderRef;

    public static View view;
    CircleImageView imageView;
    EditText edtID, edtName, edtDate, edtAddress, edtTel;
    private UploadTask mUploadTask;
    private DatabaseReference myRef;
    private ProgressDialog mProgress;

    public PersonalEditFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.personal_edit, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.fragment_personal_edit_done:
                // Do Fragment menu item stuff here
                setEditDone();

                Intent intent = new Intent(getActivity(), MainActivity.class);
                isNotificationMassage = false;
                isSetting = false;
                startActivity(intent);


                Toast.makeText(getView().getContext(), "fragment personal edit",
                        Toast.LENGTH_SHORT).show();

                return true;
            default:
                break;
        }

        return false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Firebase.Uid).child("personal");

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        view = inflater.inflate(R.layout.fragment_personal_edit, container, false);

        edtID = (EditText) view.findViewById(R.id.edt_id);
        edtName = (EditText) view.findViewById(R.id.edt_name);
        edtDate = (EditText) view.findViewById(R.id.edt_date);
        edtAddress = (EditText) view.findViewById(R.id.edt_address);
        edtTel = (EditText) view.findViewById(R.id.edt_tel);
        imageView = (CircleImageView) view.findViewById(R.id.profile_image_edit);

        mProgress.show();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String birth = dataSnapshot.child("birth").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);
                String id = dataSnapshot.child("id").getValue(String.class);

                edtID.setHint(id);
                edtName.setHint(name);
                edtDate.setHint(birth);
                edtAddress.setHint(address);
                edtTel.setHint(tel);

                imageRef = mStorageRef.child(Uid + "/profile.PNG");
                downloadDataViaUrl();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });






        final Button btnChangeImage = (Button) view.findViewById(R.id.changeImage);
        btnChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == btnChangeImage) {
                    pickImage();
                }
            }
        });

        final ImageButton btnDate = (ImageButton) view.findViewById(R.id.personal_edit_date);
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        getActivity().setTitle(getString(R.string.text_profile_edit).toUpperCase());

        return view;
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }

            try {

                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                Log.d(TAG, String.valueOf(inputStream));

                imageView.setImageDrawable(drawable);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }
    }

    public void setEditDone() {

        if (!edtID.getText().toString().equals("")) {
            setID = edtID.getText().toString();

        }
        if (!edtName.getText().toString().equals("")) {
            setName = edtName.getText().toString();
        }
        if (!edtAddress.getText().toString().equals("")) {
            setAddress = edtAddress.getText().toString();
        }
        if (!edtTel.getText().toString().equals("")) {
            setTel = edtTel.getText().toString();
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference(Uid).child("personal");

        myRef.child("name").setValue(setName);
        myRef.child("id").setValue(setID);
        myRef.child("address").setValue(setAddress);
        myRef.child("birth").setValue(setDate);
        myRef.child("tel").setValue(setTel);

        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] task = baos.toByteArray();

        mUploadTask = imageRef.putBytes(task);

        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                myRef.child("image").setValue(uri.toString());
            }
        });
    }


    private void downloadDataViaUrl() {

        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                try {
                    Glide.with(getActivity())
                            .load(uri)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(imageView);
                    mProgress.dismiss();
                } catch (Exception e) {
                    Log.d(TAG, "Error");
                }
                Log.d(TAG, String.valueOf(uri));
            }
        });
    }


}
