package asanee.tosaganjana.kku.ac.th.alzheimer.outside;

/**
 * Created by Puppymind on 27/4/2560.
 */

public class MessageModel {
    private String address;
    private String time;

    public MessageModel() {

    }

    public MessageModel(String address, String time) {
        this.address = address;
        this.time =  time;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
