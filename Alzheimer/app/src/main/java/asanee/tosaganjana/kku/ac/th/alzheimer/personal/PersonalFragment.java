package asanee.tosaganjana.kku.ac.th.alzheimer.personal;


import android.app.ProgressDialog;
import android.view.animation.Animation;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;
import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import static asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase.Uid;
import static asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity.mStorageRef;
import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.*;

import static android.content.ContentValues.TAG;


public class PersonalFragment extends Fragment {

    View view;
    CircleImageView imageView;
    StorageReference imageRef;
    private ProgressDialog mProgress;

    Thread splashTread;

    public PersonalFragment() {
        // Required empty public constructor
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.personal, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.fragment_personal_edit:

                // Do Fragment menu item stuff here

//                Intent intent = new Intent(getActivity(), PersonalEditActivity.class);
//                startActivity(intent);
                PersonalEditFragment personalEditFragment = new PersonalEditFragment();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                manager.beginTransaction().replace(
                        R.id.content_main_fragment,
                        personalEditFragment,
                        personalEditFragment.getTag()
                ).commit();

                Toast.makeText(getView().getContext(), "fragment personal edit",
                        Toast.LENGTH_SHORT).show();

                return true;

            default:
                break;
        }

        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(Firebase.Uid).child("personal");

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        mProgress.show();
        view = inflater.inflate(R.layout.fragment_personal, container, false);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name = dataSnapshot.child("name").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String birth = dataSnapshot.child("birth").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);
                String id = dataSnapshot.child("id").getValue(String.class);
                String uri = dataSnapshot.child("image").getValue(String.class);
//                try {
//                    Glide.with(getActivity())
//                            .load(uri)
//                            .priority(Priority.IMMEDIATE)
//                            .into(imageView);
//
//                } catch (Exception e) {
//                    Log.d(TAG, "Error");
//                }

                createProfile(name, id, birth, address, tel);

                imageRef = mStorageRef.child(Uid + "/profile.PNG");
                downloadDataViaUrl();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        getActivity().setTitle(getString(R.string.text_profile).toUpperCase());
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        return view;
    }

    private void createProfile(String name, String id, String birth, String address,
                               String tel) {

        TextView tvName = (TextView) view.findViewById(R.id.personal_name_tv);
        TextView tvID = (TextView) view.findViewById(R.id.personal_id_tv);
        TextView tvBirth = (TextView) view.findViewById(R.id.personal_birth_tv);
        TextView tvAddress = (TextView) view.findViewById(R.id.personal_address_tv);
        TextView tvTel = (TextView) view.findViewById(R.id.personal_tel_tv);
        imageView = (CircleImageView) view.findViewById(R.id.profile_image);

        tvName.setText(name);
        tvID.setText(id);
        tvBirth.setText(birth);
        tvAddress.setText(address);
        tvTel.setText(tel);

        setName = name;
        setID = id;
        setAddress = address;
        setTel = tel;
        setDate = birth;


    }

    private void downloadDataViaUrl() {

        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                try {
                    Glide.with(getActivity())
                            .load(uri)
                            .fitCenter()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(imageView);
                    mProgress.dismiss();
                } catch (Exception e) {
                    Log.d(TAG, "Error");
                }

                Log.d(TAG, String.valueOf(uri));
            }
        });
    }
}
