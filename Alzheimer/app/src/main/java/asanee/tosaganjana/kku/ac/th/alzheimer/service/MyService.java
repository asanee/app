package asanee.tosaganjana.kku.ac.th.alzheimer.service;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.outside.MessageModel;

import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isBreakfast;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isDinner;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isLunch;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationMassage;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationMedicine;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationOutside;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationQueue;

/**
 * Created by Puppymind on 10/4/2560.
 */

public class MyService extends Service {

    private static final String TAG = "HelloService";
    private boolean isRunning = false;
    private int number = 0;
    private DatabaseReference myRef;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    String UID;

    @Override
    public void onCreate() {
        Log.i(TAG, "Service onCreate");


        isRunning = true;

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "Service onStartCommand");

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        new Thread(new Runnable() {
            @Override
            public void run() {


                //Your logic that service will perform will be placed here
                //In this example we are just looping and waits for 1000 milliseconds in each loop.

                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }

                    if (isRunning) {


                        try {
                            UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        } catch (Exception e) {

                        }


                        final FirebaseDatabase database = FirebaseDatabase.getInstance();

                        final DatabaseReference root = database.getReference(UID);


                        root.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                /***************************************************************
                                 * Service medicine
                                 ***************************************************************
                                 */

                                DataSnapshot refMedicine = dataSnapshot.child("medicine");

                                String value = (String) refMedicine.child("number").getValue();
                                number = Integer.parseInt(value);
                                Date date = new Date();
                                DateFormat timeFormat = new SimpleDateFormat("HH.mm");
                                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                String currentTime = timeFormat.format(date);
                                String currentDate = dateFormat.format(date);

                                Double aDoubleCurrentTime = Double.parseDouble(currentTime);

                                for (int i = 0; i < number; i++) {

                                    DataSnapshot ref = refMedicine.child("list").child("id-" + (i + 1));

                                    String time = (String) ref.child("time").getValue();

                                    if (aDoubleCurrentTime > 0.00 && aDoubleCurrentTime <= 12.00) {
                                        if (contains(time, "เช้า")) {
                                            isBreakfast = true;
                                            isLunch = false;
                                            isDinner = false;
                                        }
                                    } else if (aDoubleCurrentTime > 12.00 && aDoubleCurrentTime <= 16.00) {
                                        if (contains(time, "กลางวัน")) {
                                            isBreakfast = false;
                                            isLunch = true;
                                            isDinner = false;
                                        }
                                    } else if (aDoubleCurrentTime > 16.00 && aDoubleCurrentTime <= 24.00) {
                                        if (contains(time, "เย็น")) {
                                            isBreakfast = false;
                                            isLunch = false;
                                            isDinner = true;

                                        }
                                    }

                                }

                                String timeBreakfast = (String) refMedicine.child("time").
                                        child("breakfast").getValue();
                                String timeLunch = (String) refMedicine.child("time").
                                        child("lunch").getValue();
                                String timeDinner = (String) refMedicine.child("time").
                                        child("dinner").getValue();


                                if (isNotificationMedicine) {
                                    if (NotificationStatus.isBreakfast) {
                                        if (timeBreakfast.equals(currentTime)) {
                                            showNotification("medicine", "แจ้งเตือนกินยา", "อย่าลืมกินยาตอนเช้า");
                                            isNotificationMedicine = false;
                                        }
                                    }
                                    if (isLunch) {
                                        if (timeLunch.equals(currentTime)) {
                                            showNotification("medicine", "แจ้งเตือนกินยา", "อย่าลืมกินยาตอนกลางวัน");
                                            isNotificationMedicine = false;
                                        }
                                    }
                                    if (isDinner) {
                                        if (timeDinner.equals(currentTime)) {
                                            showNotification("medicine", "แจ้งเตือนกินยา", "อย่าลืมกินยาตอนเเย็น");
                                            isNotificationMedicine = false;
                                        }
                                    }
                                }
                                /***************************************************************
                                 * Service outside
                                 ***************************************************************
                                 */

                                DataSnapshot refOutside = dataSnapshot.child("outside");
                                String leave = refOutside.child("leave").getValue(String.class);
                                String address = refOutside.child("address").getValue(String.class);

                                if (leave.equals("1") && isNotificationOutside) {
                                    showNotification("outside", "ALERT!", "สถานที่ : " + address);
                                    myRef = database.getReference(UID).child("outside");
                                    myRef.child("time").setValue(currentTime);
                                    isNotificationOutside = false;

                                    MessageModel messageModel = new MessageModel(address, currentTime);
                                    myRef.child("history").push().setValue(messageModel);

                                }


                                /***************************************************************
                                 * Service queue
                                 ***************************************************************
                                 */

                                DataSnapshot refQueue = dataSnapshot.child("queue");
                                String queueDate = refQueue.child("date").getValue(String.class);
                                String queueTime = refQueue.child("time").getValue(String.class);

                                if (currentDate.equals(queueDate) && isNotificationQueue) {
                                    showNotification("queue", "คุุณมีนัดพบแพทย์", "เวลา : " + queueTime);
                                    isNotificationQueue = false;
                                }
                                if (currentTime.equals("6.00")) {
                                    isNotificationQueue = true;
                                }

                                Log.i(TAG, "Service running");
                                Log.i(TAG, "Time " + currentTime);
                            }

                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                Log.w(TAG, "Failed to read value.", error.toException());
                            }
                        });

                    }
                }

                //Stop service once it finishes its task
                //stopSelf();
            }
        }).

                start();

        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
    }

    public void showNotification(String fragment, String title, String text) {

        isNotificationMassage = true;

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("message", fragment);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setAutoCancel(true)
                        .setSound(alarmSound)
                        .setContentIntent(pendingIntent)
                        .build();


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(generateRandom(), notification);
    }

    public boolean contains(String haystack, String needle) {
        haystack = haystack == null ? "" : haystack;
        needle = needle == null ? "" : needle;

        return haystack.toLowerCase().contains(needle.toLowerCase());
    }

    public int generateRandom() {
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }
}