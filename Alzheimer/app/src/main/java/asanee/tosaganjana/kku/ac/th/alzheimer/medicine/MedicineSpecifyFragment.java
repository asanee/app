package asanee.tosaganjana.kku.ac.th.alzheimer.medicine;


import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.medicine.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.alzheimer.medicine.index.index;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isBreakfast;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isDinner;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isLunch;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationMedicine;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicineSpecifyFragment extends Fragment implements View.OnClickListener {


    int number = 0;
    View view;
    String name;
    String beforeAfter;
    String time;
    String doseTime;
    String doseWeight;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    String active;
    List<index> dataset;

    public MedicineSpecifyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("medicine");

        view = inflater.inflate(R.layout.fragment_medicine_specify, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                isNotificationMedicine = true;
                dataset = new ArrayList<index>();

                String value = (String) dataSnapshot.child("number").getValue();
                number = Integer.parseInt(value);

                for (int i = 0; i < number; i++) {

                    DataSnapshot ref = dataSnapshot.child("list").child("id-" + (i + 1));

                    name = (String) ref.child("name").getValue();
                    beforeAfter = (String) ref.child("dose").child("beforeAfter").getValue();
                    time = (String) ref.child("time").getValue();
                    doseTime = (String) ref.child("dose").child("time").getValue();
                    doseWeight = (String) ref.child("dose").child("weight").getValue();

                    active = "";

                    if (isBreakfast) {
                        if (contains(time, "เช้า")) {
                            active = "breakfast";
                        }
                    } else if (isLunch) {
                        if (contains(time, "กลางวัน")) {
                            active = "lunch";
                        }
                    } else if (isDinner) {
                        if (contains(time, "เย็น")) {
                            active = "dinner";
                        }
                    }

                    index item = new index(name, beforeAfter, time, doseWeight, doseTime, active);
                    dataset.add(item);
                }

                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new IndexAdapter(getActivity(), initIndex());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        getActivity().setTitle("Medicine".toUpperCase());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new
                ColorDrawable(getResources().getColor(R.color.colorGreen)));



        return view;
    }

    private List<index> initIndex() {

        return dataset;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

//    @Override
////    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
////        // Do something that differs the Activity's menu here
////        super.onCreateOptionsMenu(menu, inflater);
////
////        inflater.inflate(R.menu.medicine, menu);
////
////    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.fragment_medicine_setting:

//                DialogFragment newFragment = new TimePickerFragment();
//                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");

                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                Toast.makeText(getView().getContext(), "fragment Medicine Setting",
                        Toast.LENGTH_SHORT).show();

                return true;

            case R.id.fragment_medicine_help:

                Toast.makeText(getView().getContext(), "fragment Medicine help",
                        Toast.LENGTH_SHORT).show();

                return true;

            default:
                break;
        }

        return false;
    }

    public boolean contains(String haystack, String needle) {
        haystack = haystack == null ? "" : haystack;
        needle = needle == null ? "" : needle;

        return haystack.toLowerCase().contains(needle.toLowerCase());
    }


    @Override
    public void onClick(final View view) {

        int itemPosition = mRecyclerView.getChildLayoutPosition(view);

        Toast.makeText(getActivity(), itemPosition + "", Toast.LENGTH_LONG).show();
    }
}
