package asanee.tosaganjana.kku.ac.th.alzheimer.medicine.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

public class index {
    private String name;
    private String beforeAfter;
    private String time;
    private String doseWeight;
    private String doseTime;
    private String active;

    public index(String name, String beforeAfter, String time,
                 String doseWeight, String doseTime, String active) {
        this.name = name;
        this.beforeAfter = beforeAfter;
        this.time = time;
        this.doseTime = doseTime;
        this.doseWeight = doseWeight;
        this.active = active;
    }

    public String getBeforeAfter() {
        return beforeAfter;
    }

    public void setBeforeAfter(String beforeAfter) {
        this.beforeAfter = beforeAfter;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDoseWeight() {
        return doseWeight;
    }

    public void setDoseWeight(String doseWeight) {
        this.doseWeight = doseWeight;
    }

    public String getDoseTime() {
        return doseTime;
    }

    public void setDoseTime(String doseTime) {
        this.doseTime = doseTime;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}