package asanee.tosaganjana.kku.ac.th.alzheimer.personal;

import android.app.DatePickerDialog;
import android.app.Dialog;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;

import static asanee.tosaganjana.kku.ac.th.alzheimer.personal.personal.setDate;

/**
 * Created by Puppymind on 19/4/2560.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);

//
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Log.d("onDateSet", "Date " + year + " " + month + " " + day);
        setDate = day + "-" + (month + 1) + "-" + year;
        EditText edtDate = (EditText) PersonalEditFragment.view.findViewById(R.id.edt_date);
        edtDate.setText(setDate);
    }


}