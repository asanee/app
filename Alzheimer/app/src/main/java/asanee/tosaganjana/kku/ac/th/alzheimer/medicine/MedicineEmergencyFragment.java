package asanee.tosaganjana.kku.ac.th.alzheimer.medicine;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicineEmergencyFragment extends Fragment {


    public MedicineEmergencyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_emergency, container, false);
    }

}
