package asanee.tosaganjana.kku.ac.th.alzheimer.medicine;



import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;
import asanee.tosaganjana.kku.ac.th.alzheimer.main.MainActivity;
import asanee.tosaganjana.kku.ac.th.alzheimer.medicine.index.IndexAdapter;
import asanee.tosaganjana.kku.ac.th.alzheimer.medicine.index.index;

import static android.support.v7.cardview.R.attr.cardCornerRadius;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isBreakfast;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isDinner;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isLunch;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationMassage;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isNotificationMedicine;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.service.NotificationStatus.isSetting;
import static java.security.AccessController.getContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class MedicineFragment extends AppCompatActivity implements ActionBar.TabListener {


    View view;
    private ViewPager tabsviewPager;
    private Tabsadapter mTabsAdapter;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_medicine);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);

        getSupportActionBar().setTitle(getString(R.string.text_medicine).toUpperCase());

        getSupportActionBar().setBackgroundDrawable(new
                ColorDrawable(getResources().getColor(R.color.colorGreen)));

        tabsviewPager = (ViewPager) findViewById(R.id.tabspager);

        mTabsAdapter = new Tabsadapter(getSupportFragmentManager());

        tabsviewPager.setAdapter(mTabsAdapter);

        ActionBar.Tab index = actionBar.newTab()
                .setText("ยาฉุกเฉิน")
                .setTabListener(this);


        ActionBar.Tab video = actionBar.newTab()
                .setText("ยาที่หมอสั่ง")
                .setTabListener(this);

        actionBar.addTab(index);
        actionBar.addTab(video);

        //This helps in providing swiping effect for v7 compat library
        tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                getSupportActionBar().setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.medicine, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.fragment_medicine_setting:

//                DialogFragment newFragment = new TimePickerFragment();
//                newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");

                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                Toast.makeText(this, "fragment Medicine Setting",
                        Toast.LENGTH_SHORT).show();

                return true;

            case R.id.fragment_medicine_help:

                Toast.makeText(this, "fragment Medicine help",
                        Toast.LENGTH_SHORT).show();

                return true;

            case android.R.id.home:
                Intent intent1 = new Intent(this, MainActivity.class);
                isNotificationMassage = false;
                isSetting = false;
                startActivity(intent1);
                break;
        }

        return false;
    }

    @Override
    public void onTabSelected(ActionBar.Tab selectedtab, FragmentTransaction ft) {
        tabsviewPager.setCurrentItem(selectedtab.getPosition()); //update tab position on tap
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
