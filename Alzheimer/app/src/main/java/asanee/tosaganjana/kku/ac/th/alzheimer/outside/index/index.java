package asanee.tosaganjana.kku.ac.th.alzheimer.outside.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

public class index {
    private String address;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String time;

    public index(String address, String time) {
        this.address = address;
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}