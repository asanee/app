package asanee.tosaganjana.kku.ac.th.alzheimer.queue;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.icu.util.GregorianCalendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Locale;

import asanee.tosaganjana.kku.ac.th.alzheimer.R;
import asanee.tosaganjana.kku.ac.th.alzheimer.firebase.Firebase;

import static android.content.ContentValues.TAG;
import static asanee.tosaganjana.kku.ac.th.alzheimer.R.id.toolbar;


public class QueueFragment extends Fragment {

    CompactCalendarView compactCalendarView;
    TextView tvDateCalender, tvTime, tvAppointment,tvDate;
    View view;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy",
            Locale.getDefault());

    public QueueFragment() {
        // Required empty public constructor

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.queue, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.fragment_queue_help:
                // Do Fragment menu item stuff here

                Toast.makeText(getView().getContext(), "fragment queue help",
                        Toast.LENGTH_SHORT).show();

                return true;

            default:
                break;
        }

        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(UID).child("queue");

        view = inflater.inflate(R.layout.fragment_queue, container, false);

        getActivity().setTitle(getString(R.string.text_queue).toUpperCase());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(new
                ColorDrawable(getResources().getColor(R.color.colorGreen)));


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String date = (String) dataSnapshot.child("date").getValue();
                String time = (String) dataSnapshot.child("time").getValue();
                String appointment = (String) dataSnapshot.child("appointment").getValue();

                createCalender(date,time,appointment);
            }

            @Override
            public void onCancelled(DatabaseError error) {

                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        return view;
    }

    // Change date to timestamp
    private long dateToTimestamp(String str_date) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;

        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Long timestamp = date.getTime();
        Log.d(TAG, "Today is " + timestamp);

        return timestamp;
    }

    private void createCalender(String date, String time, String appointment) {

        tvDateCalender = (TextView) view.findViewById(R.id.queue_date);
        tvTime = (TextView) view.findViewById(R.id.queue_time_tv);
        tvDate = (TextView) view.findViewById(R.id.queue_date_tv);
        tvAppointment = (TextView) view.findViewById(R.id.queue_appointment_tv);
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        // Set first day of week to Monday, defaults to Monday so calling setFirstDayOfWeek is not necessary
        // Use constants provided by Java Calendar class

        compactCalendarView.setUseThreeLetterAbbreviation(true);


        Long timestamp = dateToTimestamp(date);

        Event ev1 = new Event(Color.RED, timestamp, "test");
        compactCalendarView.addEvent(ev1, true);

        tvTime.setText(time);
        tvAppointment.setText(appointment);
        tvDate.setText(date);
        tvDateCalender.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendarView.getEvents(dateClicked);

                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                tvDateCalender.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
    }

}
