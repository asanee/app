package coe24.wdp01.com.checkover.tab;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import coe24.wdp01.com.checkover.R;
import coe24.wdp01.com.checkover.main.ClassActivity;

public class TabTestActivity extends AppCompatActivity implements ActionBar.TabListener {
    private ViewPager tabsviewPager;
    private Tabsadapter mTabsAdapter;
    String key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_test);

//        setTitle(getString(R.string.labour_pain));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//        ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.colorPrimaryLight));
//        actionBar.setStackedBackgroundDrawable(colorDrawable);

        Bundle bundle = getIntent().getExtras();
        key = bundle.getString("key");

        tabsviewPager = (ViewPager) findViewById(R.id.tabspager);

        mTabsAdapter = new Tabsadapter(getSupportFragmentManager());

        tabsviewPager.setAdapter(mTabsAdapter);

        ActionBar.Tab tab1 = actionBar.newTab()
                .setText("test1")
                .setTabListener(this);


        ActionBar.Tab tab2 = actionBar.newTab()
                .setText("test2")
                .setTabListener(this);

        ActionBar.Tab tab3 = actionBar.newTab()
                .setText("test2")
                .setTabListener(this);

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);

        //This helps in providing swiping effect for v7 compat library
        tabsviewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                getSupportActionBar().setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

    }



    @Override
    public void onTabReselected(ActionBar.Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTabSelected(ActionBar.Tab selectedtab, FragmentTransaction arg1) {
        // TODO Auto-generated method stub
        tabsviewPager.setCurrentItem(selectedtab.getPosition()); //update tab position on tap
    }

    @Override
    public void onTabUnselected(ActionBar.Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tab_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(this, ClassActivity.class);
            startActivity(intent);
        } else if(id == R.id.menu_info){
            createDialog(key);
        }

        return super.onOptionsItemSelected(item);
    }

    public void createDialog(final String key){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View viewDialog = inflater.inflate(R.layout.dialog_custom, null);
        builder.setView(viewDialog);
        TextView textView = (TextView) viewDialog.findViewById(R.id.name_dialog);
        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copy Text", key);
                clipboard.setPrimaryClip(clip);

                Toast.makeText(getApplicationContext(),"Copy to Clipboard",Toast.LENGTH_LONG).show();
                return false;
            }
        });
        textView.setText(key);
        builder.create();

        builder.show();
    }

}