package coe24.wdp01.com.checkover.main;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import coe24.wdp01.com.checkover.R;
import coe24.wdp01.com.checkover.create.RegClassActivity;
import coe24.wdp01.com.checkover.main.index.IndexAdapter;
import coe24.wdp01.com.checkover.main.index.RecyclerItemClickListener;
import coe24.wdp01.com.checkover.main.index.index;
import coe24.wdp01.com.checkover.tab.TabTestActivity;

public class ClassActivity extends AppCompatActivity implements View.OnClickListener{

    public static String keyProject;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<index> dataset;
    private ProgressDialog mProgress;
    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Processing...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        fab = (FloatingActionButton)findViewById(R.id.fab3);
        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.classList);

        mRecyclerView.setHasFixedSize(true);
        mProgress.show();;
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ClassActivity.this, RegClassActivity.class);
                startActivity(intent);

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("project");
                keyProject = myRef.push().getKey();
            }
        });

         FirebaseDatabase database = FirebaseDatabase.getInstance();
         final DatabaseReference myRef = database.getReference();
        final String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                // ...
                Toast.makeText(getApplicationContext(), String.valueOf(position +1),Toast.LENGTH_SHORT).show();


                myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        DataSnapshot user = dataSnapshot.child("user").child(UID);
                        int count = 0;
                        for (final DataSnapshot child : user.getChildren()) {
                            if(position == count){
                                String key = child.getKey();
                                Log.d("key",key);
                                Intent intent = new Intent(getApplicationContext(), TabTestActivity.class);
                                intent.putExtra("key",key);
                                startActivity(intent);
                            }
                            count++;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataset = new ArrayList<index>();


                DataSnapshot user = dataSnapshot.child("user").child(UID);
//                DataSnapshot project = dataSnapshot.
                int i = 0;
                try {
                    for (DataSnapshot child : user.getChildren()) {
                        Log.d("child", child.getKey());
                        String key = child.getValue(String.class);
                        i++;
                        String project = dataSnapshot
                                .child("project").child(key).child("create")
                                .child("name class").getValue(String.class);

                        Log.d("project", project);
                        Log.d("project", i+"");

                        index item = new index(project,i );

                        dataset.add(item);


                    }


                }catch (Exception e){

                }
                mProgress.dismiss();

                mLayoutManager = new LinearLayoutManager(getApplicationContext());
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new IndexAdapter(getApplicationContext(), initIndex());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


    }

    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }
    private List<index> initIndex() {



        return dataset;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.fab3:
                Log.d("Raj", "Fab ");
                animateFAB();
                break;
            case R.id.fab1:

                Log.d("Raj", "Fab 1");
                break;
            case R.id.fab2:

                Log.d("Raj", "Fab 2");
                break;
        }
    }
}
