package coe24.wdp01.com.checkover.tab;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import coe24.wdp01.com.checkover.R;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tab1Fragment extends Fragment {

    CompactCalendarView compactCalendarView;

    TextView tvDateCalender, tvTime, tvAppointment,tvDate;
    View view;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy",
            Locale.getDefault());

    public Tab1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tab1, container, false);

        createCalender("11-5-2017","30-5-2017");

        return view;
    }

    private void createCalender(String startDate,String finalDate) {

        tvDateCalender = (TextView) view.findViewById(R.id.queue_date);
//        tvTime = (TextView) view.findViewById(R.id.queue_time_tv);
//        tvDate = (TextView) view.findViewById(R.id.queue_date_tv);
//        tvAppointment = (TextView) view.findViewById(R.id.queue_appointment_tv);
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        // Set first day of week to Monday, defaults to Monday so calling setFirstDayOfWeek is not necessary
        // Use constants provided by Java Calendar class

        compactCalendarView.setUseThreeLetterAbbreviation(true);

        Calendar c = Calendar.getInstance();
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Date dateS = null;
        Date dateF = null;
        try {
            dateS = (Date) formatter.parse(startDate);
            dateF = (Date) formatter.parse(finalDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        while (true) {
            Long timestamp = dateS.getTime();
            Event ev1 = new Event(Color.RED, timestamp, "test");
            compactCalendarView.addEvent(ev1, true);

            if (dateF.getTime() < dateS.getTime()) {
                break;
            }
            c.setTime(dateS);
            c.add(Calendar.DATE, 7);
            dateS = c.getTime();
        }

//        Long timestamp1 = dateToTimestamp(date1);
//        Long timestamp2 = dateToTimestamp(date2);

//        Event ev1 = new Event(Color.RED, sum, "test");
//        compactCalendarView.addEvent(ev1, true);

//        Event ev2 = new Event(Color.RED, timestamp2, "test");
//        compactCalendarView.addEvent(ev2, true);
//        tvDate.setText(date);
        tvDateCalender.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = compactCalendarView.getEvents(dateClicked);

                Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                Log.d(TAG, "Month was scrolled to: " + firstDayOfNewMonth);
                tvDateCalender.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
    }

    // Change date to timestamp
    private long dateToTimestamp(String str_date) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;

        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Long timestamp = date.getTime();
        Log.d(TAG, "Today is " + timestamp);

        return timestamp;
    }
}
