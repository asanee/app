package coe24.wdp01.com.checkover.create;

/**
 * Created by Puppymind on 9/5/2560.
 */

import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

import coe24.wdp01.com.checkover.R;
import coe24.wdp01.com.checkover.main.ClassActivity;

import static coe24.wdp01.com.checkover.main.ClassActivity.keyProject;


public class RegClassActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    String nameClass;
    DatabaseReference myRef, myMaster;
    String locationShow = "";
    FirebaseDatabase database;

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_class);

        final TextView tvDateStart = (TextView) findViewById(R.id.dateStart);
        final TextView tvDateStop = (TextView) findViewById(R.id.dateStop);
        final TextView tvTimeStart = (TextView) findViewById(R.id.timeStart);
        final TextView tvTimeStop = (TextView) findViewById(R.id.timeStop);
        Button btnFinish = (Button) findViewById(R.id.btnFinish);
        final EditText edtNameClass = (EditText) findViewById(R.id.name_class);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        constantVairible();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("project")
                .child(keyProject).child("create");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    DataSnapshot date = dataSnapshot.child("date");
                    String startDate = date.child("start").getValue(String.class);
                    String finalDate = date.child("final").getValue(String.class);

                    DataSnapshot time = dataSnapshot.child("time");
                    String startTime = time.child("start").getValue(String.class);
                    String stopTime = time.child("stop").getValue(String.class);

                    tvDateStart.setText(startDate);
                    tvDateStop.setText(finalDate);
                    tvTimeStart.setText(startTime);
                    tvTimeStop.setText(stopTime);

                } catch (Exception e) {

                }


//                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        tvDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DateFragment();
                newFragment.show(getSupportFragmentManager(), "dateStart");

            }
        });


        tvDateStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DateFragment();
                newFragment.show(getSupportFragmentManager(), "dateStop");

            }
        });

        tvTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimeFragment();
                newFragment.show(getSupportFragmentManager(), "timeStart");

            }
        });

        tvTimeStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimeFragment();
                newFragment.show(getSupportFragmentManager(), "timeStop");

            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameClass = edtNameClass.getText().toString();
                myRef.child("name class").setValue(nameClass);
                checkboxDay();
                final String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                final DatabaseReference user = database.getReference("user");
                Log.d("UID", UID);
                Toast.makeText(RegClassActivity.this, UID, Toast.LENGTH_LONG).show();
                myMaster = database.getReference("project");
                myMaster.child(keyProject).child("master").setValue(UID);

                String key = user.child(UID).push().getKey();
                user.child(UID).child(key).setValue(keyProject);

                Intent intent = new Intent(getApplicationContext(), ClassActivity.class);
                startActivity(intent);

            }
        });


    }

    public void constantVairible() {

    }

    private void checkboxDay() {

        CheckBox ckMon = (CheckBox) findViewById(R.id.ckMon);
        CheckBox ckTue = (CheckBox) findViewById(R.id.ckTue);
        CheckBox ckWed = (CheckBox) findViewById(R.id.ckWed);
        CheckBox ckThu = (CheckBox) findViewById(R.id.ckThu);
        CheckBox ckFri = (CheckBox) findViewById(R.id.ckFri);
        CheckBox ckSun = (CheckBox) findViewById(R.id.ckSun);
        CheckBox ckSat = (CheckBox) findViewById(R.id.ckSat);

        if (ckMon.isChecked()) {
            myRef.child("dateOfWeek").child("Mon").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Mon").setValue("false");
        }


        if (ckTue.isChecked()) {
            myRef.child("dateOfWeek").child("Tue").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Tue").setValue("false");
        }

        if (ckWed.isChecked()) {
            myRef.child("dateOfWeek").child("Wed").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Wed").setValue("false");
        }

        if (ckThu.isChecked()) {
            myRef.child("dateOfWeek").child("Thu").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Thu").setValue("false");
        }

        if (ckFri.isChecked()) {
            myRef.child("dateOfWeek").child("Fri").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Fri").setValue("false");
        }
        if (ckSat.isChecked()) {
            myRef.child("dateOfWeek").child("Set").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Set").setValue("false");
        }

        if (ckSun.isChecked()) {
            myRef.child("dateOfWeek").child("Sun").setValue("true");
        } else {
            myRef.child("dateOfWeek").child("Sun").setValue("false");
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            Toast.makeText(this, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        myRef.child("location").child("lat").setValue(currentLatitude);
        myRef.child("location").child("log").setValue(currentLongitude);
        locationShow = getCompleteAddressString(currentLatitude, currentLongitude);
        TextView tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvLocation.setText(currentLatitude + "");

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<android.location.Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                android.location.Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current ", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current ", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current ", "Canont get Address!");
        }
        return strAdd;
    }
}
