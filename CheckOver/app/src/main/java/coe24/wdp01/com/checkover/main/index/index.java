package coe24.wdp01.com.checkover.main.index;

/**
 * Created by Puppymind on 29/4/2560.
 */

public class index {
    private String name;
    int index;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public index(String name, int index) {

        this.name = name;
        this.index = index;
    }
}