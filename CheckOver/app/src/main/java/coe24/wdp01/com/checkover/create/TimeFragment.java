package coe24.wdp01.com.checkover.create;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static coe24.wdp01.com.checkover.main.ClassActivity.keyProject;



/**
 * A simple {@link Fragment} subclass.
 */
public class TimeFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    public static String setTime;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        try {
            setTime = hourOfDay + ":" + minute;

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference myRef = database.getReference("project")
                    .child(keyProject).child("create").child("time");

            Fragment setDateStart = getFragmentManager().findFragmentByTag("timeStart");
            Fragment setDateStop = getFragmentManager().findFragmentByTag("timeStop");

            if (setDateStart != null) {
//            intent.putExtra("sentDateStart", setDate);
                myRef.child("start").setValue(setTime);
            }
            if (setDateStop != null) {
//            intent.putExtra("sentDateStop", setDate);
                myRef.child("stop").setValue(setTime);
            }


        } catch (Exception e) {

        }
    }
}


