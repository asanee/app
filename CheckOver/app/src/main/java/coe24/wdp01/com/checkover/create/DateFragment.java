package coe24.wdp01.com.checkover.create;


import android.app.DatePickerDialog;
import android.app.Dialog;

import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.DatePicker;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static coe24.wdp01.com.checkover.main.ClassActivity.keyProject;


/**
 * A simple {@link Fragment} subclass.
 */
public class DateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    String setDate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);

        //
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Log.d("onDateSet", "Date " + year + " " + month + " " + day);
        setDate = day + "-" + (month + 1) + "-" + year;

        //        Intent intent = new Intent(getActivity(), RegClassActivity.class);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("project")
                .child(keyProject).child("create").child("date");

        Fragment setDateStart = getFragmentManager().findFragmentByTag("dateStart");
        Fragment setDateStop = getFragmentManager().findFragmentByTag("dateStop");

        if (setDateStart != null) {
            //            intent.putExtra("sentDateStart", setDate);
            myRef.child("start").setValue(setDate);
        }
        if (setDateStop != null) {
            //            intent.putExtra("sentDateStop", setDate);
            myRef.child("final").setValue(setDate);
        }
        //        startActivity(intent);

    }


}
