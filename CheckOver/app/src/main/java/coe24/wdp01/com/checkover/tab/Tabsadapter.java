package coe24.wdp01.com.checkover.tab;

/**
 * Created by Puppymind on 10/5/2560.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import coe24.wdp01.com.checkover.tab.Tab1Fragment;
import coe24.wdp01.com.checkover.tab.Tab2Fragment;
import coe24.wdp01.com.checkover.tab.Tab3Fragment;

/**
 * Created by meen_ on 5/8/2017.
 */

class Tabsadapter   extends FragmentStatePagerAdapter {
    private int TOTAL_TABS = 3;

    public Tabsadapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int index) {
        // TODO Auto-generated method stub
        switch (index) {
            case 0:
                return new Tab1Fragment();

            case 1:
                return new Tab2Fragment();

            case 2:
                return new Tab3Fragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return TOTAL_TABS;
    }
}
